import { Router } from '@angular/router';
import { LocalStorageService } from 'src/app/auth/service/local-storage.service';
import { Component, OnInit } from '@angular/core';
import { CardService } from './service/card.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-bingocard',
  templateUrl: './bingoCard.component.html',
  styleUrls: ['./bingoCard.component.scss']
})
export class BingoCardComponent implements OnInit {
  card$: any = [];
  stateLoader: boolean = true;
  isLoading: boolean = true;

  constructor(
    private cardService: CardService,
    private localStorageService: LocalStorageService,
    private route: Router
  ) { }

  ngOnInit(): void {
    this.getCard();
  }

  getCard(){
    this.cardService.getCardUser()
    .subscribe(
      (card:any) => {
        const { data } = card.data;

        if(!data){
          
          Swal.fire({
            title: 'Sesión caducada',
            text: 'Tu sesión ha caducado, vuelva a ingresar por favor.',
            width: 600,
            position: 'top-end',
            showDenyButton: false,
            showConfirmButton: false,
            showCancelButton: false,
            denyButtonText: `Si, Cerrar Sesión`,
            cancelButtonText: 'No, mantenerme aquí'
          })
          
          this.localStorageService.remove("_TK");
          this.route.navigateByUrl("login");

          this.isLoading = false;
          
          return;
        }

        this.card$ = data;
        
        this.isLoading = false;
      }
    )
  }

}
